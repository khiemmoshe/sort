package handle

func BubbleSort(tosort []int) []int {
	end := len(tosort)

	for {

		if end == 0 {
			break
		}

		for i := 0; i < len(tosort)-1; i++ {

			if tosort[i] > tosort[i+1] {
				tosort[i], tosort[i+1] = tosort[i+1], tosort[i]
			}

		}
		end = end - 1
	}
	return tosort
}
